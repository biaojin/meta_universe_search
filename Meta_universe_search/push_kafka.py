# coding=utf-8
import re
import time
import pymongo
import time
import traceback
from kafka import KafkaProducer
import json
from Meta_universe_search.settings import MONGO_URI, MONGO_DB, MONGO_TABLE


class BaseClass:
    def __init__(self):
        self.producer_data = KafkaProducer(
            bootstrap_servers=['h-kafka.inner.qury.me:9092'],
            api_version=(0, 10),
            value_serializer=lambda v: json.dumps(v).encode('utf-8'),
            key_serializer=lambda v: v.encode('utf-8'),
            security_protocol='SASL_PLAINTEXT', sasl_mechanism='SCRAM-SHA-256',
            sasl_plain_username="producer", sasl_plain_password='Iex@Ac@m&9U**Q%9')

        self.mongo = pymongo.MongoClient(MONGO_URI)

    def mongo_data(self):
        try:
            m_data = self.mongo[MONGO_DB][MONGO_TABLE].find()
            return m_data
        except Exception as e:
            print(e)

    def fa_data(self, data):
        try:
            fa_name = data.get('app', '')
            value = {
                "data": {
                    "comment_count": data.get('comment_count', ''),
                    "app": data.get('app', ''),
                    "img": data.get('img', ''),
                    "query": data.get('query', ''),
                    "source": data.get('source', ''),
                    "voteup_count": data.get('voteup_count', ''),
                    "title": data.get('title', ''),
                    "version": data.get('version', ''),
                    "publish_timestamp": data.get('publish_timestamp', ''),
                    "duration": data.get('duration', ''),
                    "crawl_time": data.get('crawl_time', ''),
                    "publish_time": data.get('publish_time', ''),
                    "relay_count": data.get('relay_count', ''),
                    "subtitle": data.get('subtitle', ''),
                    "detailslink": data.get('detailslink', ''),
                    "rank": data.get('rank', ''),
                    "collection_count": data.get('collection_count', ''),
                    "browse_count": data.get('browse_count', ''),
                    "page": data.get('page', ''),
                    "category": data.get('category', ''),
                    "md5": data.get('md5', '')
                },
                "meta": {
                    "action": 1,
                    "channelId": "metaverse_20211207_10f9a1b0"
                }
            }
            if value.get('data', {}).get('title', '') and value.get('data', {}).get('detailslink', ''):
                return self.send_data2kafka(value, fa_name)
            else:
                print('条件不满足')
                return False
        except Exception as e:
            traceback.print_exc()
            print(e)

    def send_data2kafka(self, value, fa_name):
        """
        示例： kafka_producer.send('world', {'key1': 'value1'})
        :param fa_name:
        :param topic:     kafka 队列名称
        :param value:     python 字典类型
        :return:
        """
        topic = "my_test_topic"
        # print("fa_name", fa_name)
        try:
            a = self.producer_data.send(topic, key=fa_name, value=value)
            a.get(timeout=30)
            print(a, 'mongo已修改')
            return True
        except Exception as e:
            if isinstance(e, BufferError):
                print(e)
            else:
                print(traceback.print_exc())
            return False

    def close(self):
        self.producer_data.close()

    def run(self):
        num = 0
        data = self.mongo_data()
        for d in data:
            if self.fa_data(d):
                num += 1

        print('推送成功 {} 条数据'.format(num))


if __name__ == '__main__':
    b = BaseClass()
    b.run()
