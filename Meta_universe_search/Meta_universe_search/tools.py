import re
import time
import datetime
import hashlib


def time_switch(publish_time):
    publish_time = str(publish_time)
    if re.match('\d{10}', publish_time):
        pass
    if re.match('\d{13}', publish_time):
        pass
    if re.match('\d{4}-\d{2}-\d{2}', publish_time):
        pass


def clear_spaces(subtitle):
    """
    对 摘要，标题 等字符串做处理
    :param subtitle:
    :return:
    """
    subtitle = str(subtitle)
    subtitle1 = re.sub(r"<(.*?)>|/\*(.*?)\*/|amp;|\\[a-z][\d+]{3,}[a-z]|br|\\|&zwj;|简介：", '', subtitle)
    subtitle2 = re.sub('&#39;|&#039;|&lsquo;|&rsquo;|&ndash;|&apos;|nbsp|nbsp;', "'", subtitle1)
    subtitle3 = re.sub('&ldquo;|&rdquo;|&quot;', '"', subtitle2)
    return re.sub(r"\s+", ' ', subtitle3.strip())


def parse_time(date):
    """
    对 1天前，1小时前，1个月 等时间格式做处理
    :param date: 1天前
    :return: 1638521087
    """
    date = str(date).replace('发布时间：', '').replace('None', '').strip()

    if re.match('\d+个月前', date):
        date = re.match('(\d+)', date).group(1)
        date = (datetime.datetime.now() - datetime.timedelta(days=int(date) * 30)).strftime('%Y-%m-%d') + ' 00:00:00'
    if re.match('\d+月\d+日', date):
        date = re.findall('\d+', date)
        date = time.strftime('%Y', time.localtime(time.time())) + '-{}-{} 00:00:00'.format(date[0], date[1])
    if re.match('刚刚', date) or re.match('今天', date):
        date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
    if re.match('\d+分钟前', date):
        minute = re.match('(\d+)', date).group(1)
        date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time() - float(minute) * 60))
    if re.match('\d+小时前', date):
        hour = re.match('(\d+)', date).group(1)
        date = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time() - float(hour) * 60 * 60))
    if re.match('\d+天前', date):
        date = re.match('\d+', date).group(0).strip()
        date = (datetime.datetime.now() - datetime.timedelta(days=int(date))).strftime('%Y-%m-%d') + ' 00:00:00'
    if re.match('昨天.*', date):
        date = re.match('昨天(.*)', date).group(1).strip()
        date = (datetime.datetime.now() - datetime.timedelta(days=1)).strftime('%Y-%m-%d') + ' ' + date
        return parse_time(date)
    if re.match('前天.*', date):
        date = re.match('前天(.*)', date).group(1).strip()
        date = (datetime.datetime.now() - datetime.timedelta(days=2)).strftime('%Y-%m-%d') + ' ' + date
        return parse_time(date)

    if re.match('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', date):
        return re.findall('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', date)[0]
    if re.match('\d{4}-\d{2}-\d{2} \d{2}:\d{2}', date):
        return re.findall('\d{4}-\d{2}-\d{2} \d{2}:\d{2}', date)[0] + ':00'
    if re.match('\d{4}-\d{2}-\d{2}', date):
        return re.findall('\d{4}-\d{2}-\d{2}', date)[0] + ' 00:00:00'

    return date


def md5_(str):
    """
    对url进行md5加密
    :return:
    """
    md5 = hashlib.md5()
    data = str
    md5.update(data.encode('utf-8'))
    return md5.hexdigest()


def crawl_time():
    """

    :return: 当前item爬取时间戳
    """
    return int(time.time())


def get_version():
    """

    :return: 爬取时间为版本号 2021120317 到小时
    """
    return str(int(time.strftime('%Y%m%d%H', time.localtime(time.time()))))


if __name__ == '__main__':
    print(parse_time('2021-12-07 18:00'))

"""
推送kafka示例
{
    "category": "资讯",
    "crawl_time": 1638867262,
    "detailslink": "https://www.zhihu.com/question/501473602/answer/2259468599",
    "duration": "",
    "md5": "zhihu_9a4832bc7e0fa92384af18052940f3b0",
    "img": "",
    "page": 2,
    "publish_time": "2021-12-06 23:05",
    "publish_timestamp": 1638803132,
    "query": "元宇宙",
    "rank": 26,
    "relay_count": 0,
    "voteup_count": 0,
    "browse_count": 0,
    "collection_count": 0,
    "comment_count": 0,
    "app": "知乎",
    "source": "智慧",
    "subtitle": "元宇宙是典型的现代人骗造出来歪理邪说，它只能是祸乱人心，骗人的把戏。",
    "title": "元宇宙到底是什么？普通人如何从当中获益？",
    "version": "202112071654",
}

"""
