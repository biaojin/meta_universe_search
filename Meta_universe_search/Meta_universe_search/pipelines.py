# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from Meta_universe_search.tools import *
from Meta_universe_search import settings
import pymongo


class MetaUniverseSearchPipeline:
    def process_item(self, item, spider):
        # print(33333, item)
        item['title'] = clear_spaces(item.get('title', ''))
        item['subtitle'] = clear_spaces(item.get('subtitle', ''))
        item['img'] = item.get('img', '') if item.get('img', '') and item.get('img', '').startswith('http') else ''

        item['source'] = item.get('source', '').replace('来源：', '').strip() \
            if item.get('source', '') and len(item.get('source', '')) > 0 else ''

        publish_time = item.get('publish_time', '')
        if isinstance(publish_time, int) and 10 <= len(str(publish_time)) <= 13:
            if len(str(publish_time)) == 10:
                item['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(publish_time))
                item['publish_timestamp'] = publish_time
            if len(str(publish_time)) == 13:
                publish_time = int(publish_time / 1000)
                item['publish_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(publish_time))
                item['publish_timestamp'] = publish_time
        else:
            item['publish_time'] = parse_time(publish_time)
            item['publish_timestamp'] = int(time.mktime(time.strptime(str(item['publish_time']), "%Y-%m-%d %H:%M:%S"))) \
                if item['publish_time'] and len(item['publish_time']) > 0 else ''

        return item


class MongoPipeline:
    def __init__(self, mongo_url, mongo_db, mongo_table):
        self.mongo_url = mongo_url
        self.mongo_db = mongo_db
        self.mongo_table = mongo_table

    @classmethod
    def from_crawler(cls, crawler):
        return cls(mongo_url=settings.MONGO_URI,
                   mongo_db=settings.MONGO_DB,
                   mongo_table=settings.MONGO_TABLE,
                   )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_url)
        self.db = self.client[self.mongo_db]

    def process_item(self, item, spider):
        # name = item.__class__.__name__
        self.db[self.mongo_table].update_one({'md5': item.get('md5')}, {'$set': dict(item)}, upsert=True)
        return item

    def close_spider(self, spider):
        self.client.close()
