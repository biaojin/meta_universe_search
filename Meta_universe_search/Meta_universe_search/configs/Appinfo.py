app_info_dict = [
    'com.qury.baidu.zixun',
    'com.qury.baidu.video',
    'com.qury.zhihu.android',
    'com.qury.sina.news',

    # 产品 ==> 游戏
    'com.qury.crypto.voxels',
]


"""
类别（item['category'] = 'info'）
百科  pedia
行业  industry
产品  product
资讯  info
视频  video


往kafka推送数据结构
{'_type': '资讯',
 'crawl_time': 1638845092,
 'detailslink': 'https://www.zhihu.com/question/493121373/answer/2259581801',
 'duration': '',
 'md5': 'zhihu_4b70f532b795ac2628d7f51e04e1f359',
 'img': '',
 'page': 1,
 'publish_time': '2021-12-07 01:13',
 'publish_timestamp': 1638810809,
 'query': '元宇宙',
 'rank': 18,
 'search': '知乎',
 'source': '杰杰宝',
 'subtitle': '请耐心看完以下的例子，或者直接滑到最后，这位诈骗界的元宇宙带师完美诠释了元宇宙的实际应用价值 …… 2009年6月29日',
 'title': '脸书宣布计划招聘 1 万名员工开发「元宇宙」，「元宇宙」的实际应用价值有哪些？',
 'title_id': 'zhihu_49fc69e6fa67600070926d59f186eebb',
 'version': 202112071044}
 
db.collection.ensureIndex({'md5':1, 'title_id':1, 'version':1, 'publish_timestamp':1});
"""