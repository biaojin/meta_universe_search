import traceback

import scrapy
from scrapy import Request, Spider
from scrapy_redis.spiders import RedisSpider

from Meta_universe_search.items import MetaUniverseSearchItem

from Meta_universe_search.tools import *

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
}


class QuotesSpider(RedisSpider):
    name = "com.qury.baidu.zixun"
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        domain = kwargs.pop('domain', '')
        self.allowed_domains = filter(None, domain.split(','))

        super(QuotesSpider, self).__init__(*args, **kwargs)

        self.base_url = 'https://www.baidu.com/s?rtt=1&bsst=1&cl=2&tn=news&ie=utf-8&word={}&x_bfe_rqs=03E80&x_bfe_tjscore=0.100000&tngroupname=organic_news&newVideo=12&goods_entry_switch=1&rsv_dl=news_b_pn&pn={}'

    def make_requests_from_url(self, data):
        print(1111111111111, data)
        print(self.base_url.format(data, 0))
        return scrapy.Request(
            url=self.base_url.format(data, 0),
            meta={"query": data, "page": 0, "rank": 1, "version": get_version()},
            headers=headers,
            dont_filter=True,
            callback=self.parse,
        )

    def parse(self, response):
        # pass
        query = response.meta['query']
        page = response.meta['page']
        rank = response.meta['rank']
        version = response.meta['version']
        try:
            data = response.xpath('//div[@class="result-op c-container xpath-log new-pmd"]')
            for i in data:
                item = MetaUniverseSearchItem()
                item['category'] = 'info'
                item['version'] = version
                item['query'] = query
                item['title'] = ''.join(i.xpath('.//h3//text()').getall()).strip()
                item['source'] = i.xpath('.//span[@class="c-color-gray c-font-normal c-gap-right"]//text()').get()
                item['duration'] = ''
                item['publish_time'] = i.xpath('.//span[@class="c-color-gray2 c-font-normal"]//text()').get()
                item['subtitle'] = ''.join(i.xpath('.//span[@class="c-font-normal c-color-text"]//text()').getall()).strip()
                item['detailslink'] = i.xpath('.//h3//@href').get()
                item['img'] = i.xpath('.//div[@class="c-img c-img3 c-img-radius-large"]//img//@src').get()
                item['app'] = 'com.qury.baidu.zixun'
                item['page'] = page
                item['rank'] = rank
                item['md5'] = 'baiduzixun_' + md5_(item['detailslink'])
                item['title_id'] = 'baiduzixun_' + md5_(item['title'])
                item['Push_status'] = 0
                item['crawl_time'] = crawl_time()

                item['comment_count'] = 0  # - - 评论数
                item['voteup_count'] = 0  # - - 点赞数
                item['collection_count'] = 0  # - - 收藏数
                item['relay_count'] = 0  # - - 转发数
                item['browse_count'] = 0  # - - 浏览数

                if item['title'] and item['detailslink']:
                    rank += 1
                    yield item
                else:
                    pass
                    # return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)

            if page <= 10 and len(data) != 0:
                page += 1
                yield scrapy.Request(
                    url=self.base_url.format(query, page * 10),
                    meta={"query": query, "page": page, "rank": rank, "version": version},
                    headers=headers,
                    dont_filter=True,
                    callback=self.parse,
                )
        except Exception as e:
            print(traceback.format_exc())


if __name__ == '__main__':
    from scrapy import cmdline

    cmdline.execute("scrapy crawl com.qury.baidu.zixun".split())
