import traceback

import scrapy
from scrapy import Request, Spider
from scrapy_redis.spiders import RedisSpider

from Meta_universe_search.items import MetaUniverseSearchItem

from Meta_universe_search.tools import *

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
}


class QuotesSpider(RedisSpider):
    name = "com.qury.sina.news"
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        domain = kwargs.pop('domain', '')
        self.allowed_domains = filter(None, domain.split(','))

        super(QuotesSpider, self).__init__(*args, **kwargs)

        self.base_url = 'https://search.sina.com.cn/news?q={}&c=news&range=all&size=10&page={}'

    def make_requests_from_url(self, data):
        print(1111111111111, data)
        return scrapy.Request(
            url=self.base_url.format(data, 1),
            meta={"query": data, "page": 1, "rank": 1, "version": get_version()},
            headers=headers,
            dont_filter=True,
            callback=self.parse,
        )

    def parse(self, response):
        # pass
        query = response.meta['query']
        page = response.meta['page']
        rank = response.meta['rank']
        version = response.meta['version']
        try:
            data = response.xpath('//div[@class="box-result clearfix"]')
            for i in data:
                item = MetaUniverseSearchItem()
                item['category'] = 'info'
                item['app'] = 'com.qury.sina.news'
                item['page'] = page
                item['rank'] = rank
                item['version'] = version
                item['query'] = query
                item['duration'] = ''
                item['Push_status'] = 0
                item['crawl_time'] = crawl_time()

                item['title'] = ''.join(i.xpath('.//h2//a//text()').getall()).strip()
                source = i.xpath('.//h2//span//text()').get()
                p_time = re.findall('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', source)
                if len(p_time) == 0:
                    source = source.split()
                    item['source'] = source[0].strip() if source and len(source) > 0 else ''
                    item['publish_time'] = source[1].strip() if source and len(p_time) > 1 else ''
                elif len(p_time) != 0:
                    item['source'] = source.replace(' ', '').replace(p_time[0], '')
                    item['publish_time'] = p_time[0].strip()
                else:
                    item['source'] = ''
                    item['publish_time'] = ''

                item['subtitle'] = ''.join(i.xpath('.//p//text()').getall()).strip()
                item['detailslink'] = i.xpath('.//h2//a//@href').get()
                item['img'] = i.xpath('.//div[@class="r-img"]//img//@src').get()

                item['md5'] = 'xinlang_' + md5_(item['detailslink'])
                item['title_id'] = 'xinlang_' + md5_(item['title'])

                item['comment_count'] = 0  # - - 评论数
                item['voteup_count'] = 0  # - - 点赞数
                item['collection_count'] = 0  # - - 收藏数
                item['relay_count'] = 0  # - - 转发数
                item['browse_count'] = 0  # - - 浏览数

                if item['title'] and item['detailslink']:
                    rank += 1
                    yield item
                else:
                    pass
                    # return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)

            if page <= 10 and len(data) != 0:
                page += 1
                yield scrapy.Request(
                    url=self.base_url.format(query, page),
                    meta={"query": query, "page": page, "rank": rank, "version": version},
                    headers=headers,
                    dont_filter=True,
                    callback=self.parse,
                )
        except Exception as e:
            print(traceback.format_exc())


if __name__ == '__main__':
    from scrapy import cmdline

    cmdline.execute("scrapy crawl com.qury.sina.news".split())
