import re
import traceback
from urllib import parse

import execjs
import scrapy
from scrapy import Request, Spider
from scrapy_redis.spiders import RedisSpider

from Meta_universe_search.items import MetaUniverseSearchItem
from Meta_universe_search.tools import *

cookie = '_zap=51bc02e8-70ac-4476-b6a5-ca52554c63a2; _xsrf=a3108937-0be3-459f-bff5-39e6ba20c8f8; d_c0="AIAQwprYHxSPTv597kLH2JJu_-T4zKCK-fg=|1638514540"; KLBRSID=57358d62405ef24305120316801fd92a|1638514539|1638514539'
headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36',
    'x-zse-93': '101_3_2.0',
    'cookie': cookie,
}


def get_x_zsc_96(url):
    url1 = url.replace('https://www.zhihu.com', '')
    x_zse_93 = "101_3_2.0"
    cookie_d_c0 = '{}'.format(re.findall('d_c0=(.*?);', cookie)[0])
    f = "+".join([x_zse_93, url1, cookie_d_c0])
    fmd5 = md5_(f)
    with open(r'../json_file/zhuhu.js', 'r') as f:
        text = f.read()
    ctx1 = execjs.compile(text)
    encrypt_str = ctx1.call('b', fmd5)
    return encrypt_str


class QuotesSpider(RedisSpider):
    name = "com.qury.zhihu.android"
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        domain = kwargs.pop('domain', '')
        self.allowed_domains = filter(None, domain.split(','))

        super(QuotesSpider, self).__init__(*args, **kwargs)

        self.base_url = 'https://www.zhihu.com/api/v4/search_v3?t=general&q={}&correction=1&offset={}&limit=20&filter_fields=&lc_idx={}&show_all_topics=0&search_source=Filter&time_interval=a_day'

    def make_requests_from_url(self, data):
        print(1111111111111, data)
        url = self.base_url.format(parse.quote(data), 0, 0)
        headers['x-zse-96'] = '2.0_%s' % get_x_zsc_96(url)
        headers['referer'] = 'https://www.zhihu.com/search?q={}&type=content&utm_content=search_suggestion&time_interval=a_day'.format(parse.quote(data))
        return scrapy.Request(
            url=url,
            meta={"query": data, "page": 1, "rank": 1, "version": get_version()},
            headers=headers,
            dont_filter=True,
            callback=self.parse,
        )

    def parse(self, response):
        # pass
        query = response.meta['query']
        page = response.meta['page']
        rank = response.meta['rank']
        version = response.meta['version']
        try:
            response = response.json()
            next_id = response.get('search_action_info', {}).get('search_hash_id', '')
            data = response.get('data', [])
            for i in data:
                item = MetaUniverseSearchItem()
                item['category'] = 'info'
                item['version'] = version
                item['query'] = query
                item['app'] = 'com.qury.zhihu.android'
                item['page'] = page
                item['rank'] = rank
                item['duration'] = ''
                item['Push_status'] = 0
                item['crawl_time'] = crawl_time()

                item['title'] = i.get('highlight', {}).get('title', '')
                item['source'] = i.get('object', {}).get('author', {}).get('name', '')
                item['publish_time'] = i.get('object', {}).get('updated_time', '')
                item['subtitle'] = i.get('highlight', {}).get('description', '')
                u_id0 = i.get('object', {}).get('id', '')
                u_id1 = i.get('object', {}).get('question', {}).get('id', '')
                item['detailslink'] = 'https://www.zhihu.com/question/{}/answer/{}'.format(u_id1,
                                                                                           u_id0) if u_id0 and u_id1 else ''
                img = i.get('object', {}).get('thumbnail_info', {}).get('thumbnails', '')
                item['img'] = img[0].get('url', '') if img and isinstance(img, list) and len(img) > 0 else ''
                item['md5'] = 'zhihu_' + md5_(item['detailslink'])
                item['title_id'] = 'zhihu_' + md5_(item['title'])
                item['voteup_count'] = i.get('object', {}).get('voteup_count', '')
                item['comment_count'] = i.get('object', {}).get('comment_count', '')
                item['collection_count'] = 0  # - - 收藏数
                item['relay_count'] = 0  # - - 转发数
                item['browse_count'] = 0  # - - 浏览数

                if item['title'] and item['detailslink']:
                    rank += 1
                    yield item
                else:
                    pass
                    # return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)
            #
            if page <= 10 and next_id is not None:
                page += 1
                url = self.base_url.format(parse.quote(query), page*20, page*20) + next_id
                headers['x-zse-96'] = '2.0_%s' % get_x_zsc_96(url)
                headers['referer'] = 'https://www.zhihu.com/search?q={}&type=content&utm_content=search_suggestion&time_interval=a_day'.format(parse.quote(query))
                yield scrapy.Request(
                    url=url,
                    meta={"query": query, "page": page, "rank": rank, "version": version},
                    headers=headers,
                    dont_filter=True,
                    callback=self.parse,
                )
        except Exception as e:
            print(traceback.format_exc())


if __name__ == '__main__':
    from scrapy import cmdline

    cmdline.execute("scrapy crawl com.qury.zhihu.android".split())
