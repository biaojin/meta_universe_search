import traceback

import scrapy
from scrapy import Request, Spider
from scrapy_redis.spiders import RedisSpider

from Meta_universe_search.items import MetaUniverseSearchItem

from Meta_universe_search.tools import *

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
}


class QuotesSpider(RedisSpider):
    name = "com.qury.baidu.video"
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        domain = kwargs.pop('domain', '')
        self.allowed_domains = filter(None, domain.split(','))

        super(QuotesSpider, self).__init__(*args, **kwargs)

        self.base_url = 'https://www.baidu.com/sf/vsearch?pd=video&tn=vsearch&ie=utf-8&wd={}&rsv_spt=13&rsv_bp=1&f=8&async=1&pn={}'

    def make_requests_from_url(self, data):
        print(1111111111111, data)
        print(self.base_url.format(data, 0))
        return scrapy.Request(
            url=self.base_url.format(data, 0),
            meta={"query": data, "page": 0, "rank": 1, "version": get_version()},
            headers=headers,
            dont_filter=True,
            callback=self.parse,
        )

    def parse(self, response):
        # pass
        query = response.meta['query']
        page = response.meta['page']
        rank = response.meta['rank']
        version = response.meta['version']
        try:
            data = response.xpath('//div[@class="video_list video_short"]')
            for i in data:
                item = MetaUniverseSearchItem()
                item['category'] = 'video'
                item['app'] = 'com.qury.baidu.video'
                item['version'] = version
                item['query'] = query
                item['rank'] = rank
                item['page'] = page
                item['crawl_time'] = crawl_time()
                item['Push_status'] = 0
                item['title'] = ''.join(i.xpath('.//div//a//text()').getall()).strip()
                item['source'] = i.xpath('.//span[@class="wetSource c-font-normal"]//text()').get()
                item['duration'] = i.xpath('.//span[@class="video_play_timer"]//text()').get()
                item['publish_time'] = i.xpath('.//span[@class="c-font-normal"]//text()').get()
                item['subtitle'] = ''.join(i.xpath('.//div[@class="c-color-text c-font-normal"]//text()').getall()).strip()
                item['detailslink'] = i.xpath('./a//@href').get()
                item['img'] = i.xpath('./a/div//img//@src').get()
                item['md5'] = 'baiduvideo_' + md5_(item['detailslink'])
                item['title_id'] = 'baiduvideo_' + md5_(item['title'])

                item['comment_count'] = 0  # - - 评论数
                item['voteup_count'] = 0  # - - 点赞数
                item['collection_count'] = 0  # - - 收藏数
                item['relay_count'] = 0  # - - 转发数
                item['browse_count'] = 0  # - - 浏览数

                if item['title'] and item['detailslink']:
                    rank += 1
                    yield item
                else:
                    pass
                    # return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)

            if page <= 10:
                page += 1
                yield scrapy.Request(
                    url=self.base_url.format(query, page * 10),
                    meta={"query": query, "page": page, "rank": rank, "version": version},
                    headers=headers,
                    dont_filter=True,
                    callback=self.parse,
                )
        except Exception as e:
            print(traceback.format_exc())


if __name__ == '__main__':
    from scrapy import cmdline

    cmdline.execute("scrapy crawl com.qury.baidu.video".split())

