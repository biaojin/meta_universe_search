import scrapy


class Movie80Spider(scrapy.Spider):
    name = 'movie80'  # 应用名称
    allowed_domains = ['80s.tw']  # 允许爬取的域名，需要手动修改（如果遇到非该域名的url则爬取不到数据，但是对于start_urls里的起始爬取页面，它是不会过滤的）
    start_urls = ['https://www.80s.tw/movie/list/']  # 起始爬取的url

    # 访问起始URL并获取结果后的回调函数，该函数的response参数就是向起始的url发送请求后，获取的响应对象.该函数返回值必须为可迭代对象或者NUll
    def parse(self, response):
        print("爬虫开始运行,结果如下————")
        print(response)  # 获取字符串类型的响应内容


if __name__ == '__main__':
    from scrapy import cmdline
    cmdline.execute("scrapy crawl movie80".split())