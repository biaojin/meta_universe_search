import traceback

import scrapy
from scrapy import Request, Spider
from scrapy_redis.spiders import RedisSpider

from Meta_universe_search.items import MetaUniverseSearchItem

from Meta_universe_search.tools import *

headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
}


class QuotesSpider(RedisSpider):
    name = "com.qury.crypto.voxels"
    redis_key = f'{name}:start_urls'

    def __init__(self, *args, **kwargs):
        domain = kwargs.pop('domain', '')
        self.allowed_domains = filter(None, domain.split(','))

        super(QuotesSpider, self).__init__(*args, **kwargs)

        self.base_url = 'https://www.cryptovoxels.com/parcels?q={}'

    def make_requests_from_url(self, data):
        print(1111111111111, data)
        print(self.base_url.format(data, 0))
        return scrapy.Request(
            url=self.base_url.format(data, 0),
            meta={"query": data, "page": 0, "rank": 1, "version": get_version()},
            headers=headers,
            dont_filter=True,
            callback=self.parse_url,
        )

    def parse_url(self, response):
        query = response.meta['query']
        page = response.meta['page']
        rank = response.meta['rank']
        version = response.meta['version']
        try:
            response = response.xpath('//tr[@class="property"]')
            if len(response) == 0:
                print('query {} 没有数据'.format(query))
            else:
                for i in response:
                    rank += 1
                    link_ = i.xpath('.//a//@href').getall()
                    link_url = 'https://www.cryptovoxels.com{}'.format(link_[0]) if link_ and len(link_) > 0 else ''
                    yield scrapy.Request(
                        url=link_url,
                        meta={"query": query, "page": page, "rank": rank, "version": version},
                        headers=headers,
                        dont_filter=True,
                        callback=self.parse,
                    )
        except Exception as e:
            print(traceback.format_exc())

    def parse(self, response):
        # pass
        query = response.meta['query']
        page = response.meta['page']
        rank = response.meta['rank']
        version = response.meta['version']
        try:
            item = MetaUniverseSearchItem()
            item['category'] = 'product'
            item['version'] = version
            item['query'] = query
            item['duration'] = ''
            item['publish_time'] = ''
            item['img'] = ''
            item['app'] = 'com.qury.crypto.voxels'
            item['page'] = page
            item['rank'] = rank
            item['Push_status'] = 0
            item['crawl_time'] = crawl_time()
            item['comment_count'] = 0  # - - 评论数
            item['voteup_count'] = 0  # - - 点赞数
            item['collection_count'] = 0  # - - 收藏数
            item['relay_count'] = 0  # - - 转发数

            item['title'] = ''.join(response.xpath('.//h1//text()').getall()).strip()
            item['subtitle'] = ''.join(response.xpath('.//h2//text()').getall()).strip()
            data_key = response.xpath('//dl[@class="attributes"]//dt')
            data_value = response.xpath('//dl[@class="attributes"]//dd')
            if len(data_key) == len(data_value):
                for k in range(len(data_key)):
                    txt = data_key[k].xpath('.//text()').getall()
                    if txt and len(txt) > 0 and 'Owner' in txt[0]:
                        item['source'] = ''.join(data_value[k].xpath('.//text()').getall()).strip()

                    if txt and len(txt) > 0 and 'Location' in txt[0]:
                        item['detailslink'] = 'https://www.cryptovoxels.com/play?coords={}'.format(''.join(data_value[k].xpath('.//text()').getall())).strip() if 'Location' in txt else ''

                    if txt and len(txt) > 0 and 'Traffic' in txt[0]:
                        item['browse_count'] = ''.join(data_value[k].xpath('.//text()').getall()).strip().replace(' Visits', '')  # - - 浏览数

            item['md5'] = 'Cryptovoxels_' + md5_(item['detailslink'])
            item['title_id'] = 'Cryptovoxels_' + md5_(item['title'])

            if item['title'] and item['detailslink']:
                rank += 1
                yield item
            else:
                pass
                # return_item_error(app_package_id=self.name, query=query, text="Condition filtering", vers=vers)

        except Exception as e:
            print(traceback.format_exc())


if __name__ == '__main__':
    from scrapy import cmdline

    cmdline.execute("scrapy crawl com.qury.crypto.voxels".split())
