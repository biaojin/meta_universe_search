# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class MetaUniverseSearchItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    category = scrapy.Field()  # - - 数据类型（资讯 / 视频）
    version = scrapy.Field()  # - - 版本号（根据每天时间自动生成）
    query = scrapy.Field()  # - - 搜索词
    title = scrapy.Field()  # - - 标题
    source = scrapy.Field()  # - - 来源
    duration = scrapy.Field()  # - - 视频时长
    publish_time = scrapy.Field()  # - - 发布时间(格式统一)
    publish_timestamp = scrapy.Field()  # - - 发布时间戳
    subtitle = scrapy.Field()  # - - 摘要（简介）
    detailslink = scrapy.Field()  # - - 详情页链接
    img = scrapy.Field()  # - - 图片
    app = scrapy.Field()  # - - 搜索来源
    md5 = scrapy.Field()  # - - 根据details_url字段做MD5加密
    title_id = scrapy.Field()  # - - 根据title字段做MD5加密
    Push_status = scrapy.Field()  # - - 推送状态
    crawl_time = scrapy.Field()  # - - 抓取时间戳
    page = scrapy.Field()  # - - 翻页
    rank = scrapy.Field()  # - - 条数

    comment_count = scrapy.Field()  # - - 评论数
    voteup_count = scrapy.Field()  # - - 点赞数
    collection_count = scrapy.Field()  # - - 收藏数
    relay_count = scrapy.Field()  # - - 转发数
    browse_count = scrapy.Field()  # - - 浏览数
    pass
